// Server side
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client"));

//const NODE_PORT = process.env.NODE_PORT || 3000;
const NODE_PORT = process.env.PORT || 3000 ;
if (!NODE_PORT) {
    console.log("Express Port is not set");
    process.exit();
}

var quizes = [
{
    id:0,
    question: "2*2*2=?",
    answer1: {name: "6", value: 1},
    answer2: {name: "8", value: 2},
    answer3: {name: "10", value: 3},
    answer4: {name: "2", value: 4},
    answer5: {name: "18", value: 5},
    correctanswer: 2
},
{
    id:1,
    question: "2*2+2=?",
    answer1: {name: "6", value: 1},
    answer2: {name: "8", value: 2},
    answer3: {name: "10", value: 3},
    answer4: {name: "2", value: 4},
    answer5: {name: "18", value: 5},
    correctanswer: 1
},
{
    id:2,
    question: "2+2*2+2+2=?",
    answer1: {name: "6", value: 1},
    answer2: {name: "8", value: 2},
    answer3: {name: "10", value: 3},
    answer4: {name: "2", value: 4},
    answer5: {name: "18", value: 5},
    correctanswer: 3
},
{
    id:3,
    question: "2*2*2/2=?",
    answer1: {name: "6", value: 1},
    answer2: {name: "8", value: 2},
    answer3: {name: "10", value: 3},
    answer4: {name: "4", value: 4},
    answer5: {name: "18", value: 5},
    correctanswer: 4
},
{
    id:4,
    question: "(2+2)*2*2=?",
    answer1: {name: "6", value: 1},
    answer2: {name: "8", value: 2},
    answer3: {name: "10", value: 3},
    answer4: {name: "2", value: 4},
    answer5: {name: "16", value: 5},
    correctanswer: 5
}
];

app.get("/popquizes", function(req,res) {
    var x = Math.random() * (6-1) + 1;
    var y = Math.floor(x);
    console.log(y);
    res.json(quizes[y-1]);
});

app.post("/submit-quiz", function(req,res){
    var quiz = req.body;
    var checking = quizes[quiz.id];
    if (checking.correctanswer == parseInt(quiz.value)) {
        quiz.isCorrect = true;
    } else {
        quiz.isCorrect = false;
    }
    res.status(200).json(quiz);
});
/*
app.get("/", function (req, res) {
    res.send('hello world')
})

*/
app.use(function(req, res) {
    console.log(" Page not found -> ");
    res.send("<h1>! ! Page not found ! ! </h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});