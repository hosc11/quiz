
## create /client/index.html
1. <html lang="en" ng-app="AppName">

```change "AppName" to your own Appication Name, example: <html lang="en" ng-app="PopQuizApp">
```

```Same "AppName" will be used in /client/app.js,
example: var app = angular.module("PopQuizApp", []);
```

2. <body ng-cloak>
`is used to prevent the AngularJS html template from being briefly displayed by the browser in its raw (uncompiled) form while your application is loading. Use this directive to avoid the undesirable flicker effect caused by the html template display.`

3. <div ng-controller="AppController as ctrl" AppName-page">
`Using controller as makes it obvious which controller you are accessing in the template when multiple controllers apply to an element. `
`change "AppController" to your own Application Name Controller
and
"AppName-page" to your own Application Page name
example: <div ng-controller="PopQuizCtrl as ctrl" class="col-sm-8 col-lg-4 popquiz-page">`

4. `<form name="popquizForm" ng-submit="ctrl.submitQuiz()" novalidate>`

